#ifndef OBCHOD_H
#define OBCHOD_H

#include <QtWidgets/QMainWindow>
#include "ui_obchod.h"
#include "zoznam_tovaru.h"

class obchod : public QMainWindow
{
	Q_OBJECT

public:
	obchod(QWidget *parent = 0);
	~obchod();
public slots:
	void buton_qliq();
	void pokladna(double value);
signals:
	void tovaruj(int pocet);

private:
	Ui::obchodClass ui;
	zoznam_tovaru zoznam;
};

#endif // OBCHOD_H
