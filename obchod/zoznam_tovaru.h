﻿#pragma once
#include <QWidget>
#include "ui_zoznam_tovaru.h"
#include <QMessageBox>

class zoznam_tovaru : public QWidget {
	Q_OBJECT

public:
	zoznam_tovaru(QWidget * parent = Q_NULLPTR);
	~zoznam_tovaru();
public slots:
	void batn_klik();
	void tovar(int pocet);
signals:
	void klik(double value);
private:
	Ui::zoznam_tovaru ui;
	QHash<QString, double> tovary;
	double suma;
};
