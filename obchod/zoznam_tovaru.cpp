﻿#include "zoznam_tovaru.h"

zoznam_tovaru::zoznam_tovaru(QWidget * parent) : QWidget(parent) {
	ui.setupUi(this);
	tovary.insert("vajcia", 1.5);
	tovary.insert("chlieb", 8.3);
	tovary.insert("muka", 2.5);
	tovary.insert("mlieko", 20.5);
	tovary.insert("psie zradlo", 6.1);
	qsrand(20);
}

zoznam_tovaru::~zoznam_tovaru() {
	
}

void zoznam_tovaru::tovar(int pocet)
{
	suma = 0;
	ui.listWidget->clear();
	for (int i = 0; i < pocet; i++)
	{
		int kus = qrand() % 5 + 1;
		double cena = tovary[tovary.keys()[i]] * kus;
		suma += cena;
		ui.listWidget->addItem("Tovar: " + tovary.keys()[i] + " pocet kusov: " + QString::number(kus) + " celkova cena: " + QString::number(cena));
	}
}

void zoznam_tovaru::batn_klik()
{
	emit klik(suma);
}