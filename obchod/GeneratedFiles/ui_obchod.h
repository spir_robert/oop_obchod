/********************************************************************************
** Form generated from reading UI file 'obchod.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OBCHOD_H
#define UI_OBCHOD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_obchodClass
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QSpinBox *spinBox;
    QLabel *label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *obchodClass)
    {
        if (obchodClass->objectName().isEmpty())
            obchodClass->setObjectName(QStringLiteral("obchodClass"));
        obchodClass->resize(600, 400);
        centralWidget = new QWidget(obchodClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(50, 60, 75, 23));
        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(70, 20, 42, 22));
        spinBox->setMinimum(1);
        spinBox->setMaximum(5);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(70, 120, 191, 21));
        obchodClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(obchodClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        obchodClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(obchodClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        obchodClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(obchodClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        obchodClass->setStatusBar(statusBar);

        retranslateUi(obchodClass);
        QObject::connect(pushButton, SIGNAL(clicked()), obchodClass, SLOT(buton_qliq()));

        QMetaObject::connectSlotsByName(obchodClass);
    } // setupUi

    void retranslateUi(QMainWindow *obchodClass)
    {
        obchodClass->setWindowTitle(QApplication::translate("obchodClass", "obchod", 0));
        pushButton->setText(QApplication::translate("obchodClass", "PushButton", 0));
        label->setText(QApplication::translate("obchodClass", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class obchodClass: public Ui_obchodClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OBCHOD_H
