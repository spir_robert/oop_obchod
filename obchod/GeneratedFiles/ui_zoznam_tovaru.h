/********************************************************************************
** Form generated from reading UI file 'zoznam_tovaru.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ZOZNAM_TOVARU_H
#define UI_ZOZNAM_TOVARU_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_zoznam_tovaru
{
public:
    QListWidget *listWidget;
    QPushButton *pushButton;

    void setupUi(QWidget *zoznam_tovaru)
    {
        if (zoznam_tovaru->objectName().isEmpty())
            zoznam_tovaru->setObjectName(QStringLiteral("zoznam_tovaru"));
        zoznam_tovaru->resize(400, 300);
        listWidget = new QListWidget(zoznam_tovaru);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 10, 371, 211));
        pushButton = new QPushButton(zoznam_tovaru);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(10, 260, 75, 23));

        retranslateUi(zoznam_tovaru);
        QObject::connect(pushButton, SIGNAL(clicked()), zoznam_tovaru, SLOT(batn_klik()));

        QMetaObject::connectSlotsByName(zoznam_tovaru);
    } // setupUi

    void retranslateUi(QWidget *zoznam_tovaru)
    {
        zoznam_tovaru->setWindowTitle(QApplication::translate("zoznam_tovaru", "zoznam_tovaru", 0));
        pushButton->setText(QApplication::translate("zoznam_tovaru", "PushButton", 0));
    } // retranslateUi

};

namespace Ui {
    class zoznam_tovaru: public Ui_zoznam_tovaru {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ZOZNAM_TOVARU_H
