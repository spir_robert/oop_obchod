#include "obchod.h"

obchod::obchod(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	zoznam.show();
	connect(this, &obchod::tovaruj, &zoznam, &zoznam_tovaru::tovar);
	connect(&zoznam, &zoznam_tovaru::klik,this,&obchod::pokladna);
}

obchod::~obchod()
{

}
void obchod::pokladna(double value)
{
	ui.label->setText("celkova cena je: " + QString::number(value));
}
void obchod::buton_qliq()
{
	emit tovaruj(ui.spinBox->value());
}
